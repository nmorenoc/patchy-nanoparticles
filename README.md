# Patchy nanoparticles

Database of patchy nanoparticles

This repository contains LAMMPS data files with the coordinates of different  stabilised patchy nanoparticles made of triblock terpolymers ABC in selective solvents. 